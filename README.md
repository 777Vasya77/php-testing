# Тестовое задание на вакансию PHP разработчика #

Ваша задача, клонировать этот репозиторий с простой версткой, и написать под нее админку, на чистом PHP! Реализовать необходимо стандартные CRUD методы:

1. Create (на Ваше усмотрение) 
2. Read (просмотр конкретного товара по клику на заголовок, шаблон есть в репозитории "single-page")
3. Update/Edit
4. Delete

**НЕ ОБЯЗАТЕЛЬНО:** если Вы понимаете и умеете, реализуйте отправку письма, при клике на кнопку "Купить", на email, с инфой по выбранному товару!

Результат выполнения тестового задания запуште себе в репозиторий и пришлите нам ссылку. Мы обязательно посмотрим и свяжемся с Вами!

P.S: Дамп базы пуште в репо тоже)